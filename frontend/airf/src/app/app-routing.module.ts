import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {UserPageComponent} from "./pages/user-page/user-page.component";
import {BagsComponent} from "./pages/bags/bags.component";
import {UserFormComponent} from "./shared/components/user-creation/user-form.component";

const routes: Routes = [
  {
    path: 'users',
    component: UserPageComponent
  },
  {
    path: 'bags/:id',
    component: BagsComponent
  },
  {
    path: '', redirectTo: 'users', pathMatch: 'full'
  },
  {
    path: '**', redirectTo: 'users'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { Component, OnInit } from '@angular/core';
import {User} from "../../core/models/user.model";
import {UserService} from "../../core/services/user.service";
import {Gender} from "../../shared/enums/gender.enum";
import {MatDialog} from "@angular/material/dialog";
import {UserFormComponent} from "../../shared/components/user-creation/user-form.component";
import {MatTableDataSource} from "@angular/material/table";

@Component({
  selector: 'airf-user-page',
  templateUrl: './user-page.component.html',
  styleUrls: ['./user-page.component.scss']
})
export class UserPageComponent implements OnInit {

  constructor(
    private userService: UserService,
    private dialog: MatDialog
  ) {

  }

  currentUser: User = {} as User;
  usersDatasource = new MatTableDataSource<User>();
  //   [
  //     {
  //       birthdate: new Date('12/12/2000'),
  //       gender: Gender.MR,
  //       id: 0,
  //       phoneNumber: '15314684',
  //       residenceCountry: 'string',
  //       username: 'dave 0'
  //     },
  //     {
  //       birthdate: new Date('12/12/2000'),
  //       gender: Gender.MR,
  //       id: 1,
  //       phoneNumber: '15314684',
  //       residenceCountry: 'string',
  //       username: 'moi je 1'
  //     },
  //     {
  //       birthdate: new Date('12/12/2000'),
  //       gender: Gender.MR,
  //       id: 2,
  //       phoneNumber: '15314684',
  //       residenceCountry: 'string',
  //       username: 'supa supa 2'
  //     },
  //     {
  //       birthdate: new Date('12/12/2000'),
  //       gender: Gender.MR,
  //       id: 3,
  //       phoneNumber: '15314684',
  //       residenceCountry: 'string',
  //       username: 'moi je 3'
  //     },
  //     {
  //       birthdate: new Date('12/12/2000'),
  //       gender: Gender.MR,
  //       id: 4,
  //       phoneNumber: '15314684',
  //       residenceCountry: 'string',
  //       username: 'supa supa 4'
  //     },
  //   ]
  // );
  columnsToDisplay: Array<string> = ['id', 'username', 'operations'];

  ngOnInit(): void {
    // this.usersDatasource.data = ;
    // this.usersDatasource.data = this.users;
    // this.userService.getAllUsers()
    //   .pipe(takeUntil(this._destroy))
    //   .subscribe(
    //     (user: User) => {
    //       this.users.add(user);
    //     }
    //   );
    this.userService.getAllUsers().subscribe(
      (response) => {
        console.log(response);
        console.log(response.status);
        if (response.body !== null) {
          // @ts-ignore
          this.usersDatasource.data.push(...response);
          this.usersDatasource.connect().next(this.usersDatasource.data);
        }
      }
    );

  }

  deleteUser(id: number): void {
    this.usersDatasource.data = this.usersDatasource.data.filter(user => user.id !== id);
    this.userService.deleteUser(id);
    this.usersDatasource.connect().next(this.usersDatasource.data);

  }

  editUser(user: User): void {
    console.log(user);
    const dialogRef = this.dialog.open(UserFormComponent, {
      width: '550px',
      data: {user:user}
    });

    dialogRef.afterClosed().subscribe(user => {
      this.userService.updateUser(user);
      this.usersDatasource.data[this.getUserIndex(user)] = user;
      this.usersDatasource.connect().next(this.usersDatasource.data);
    });

  }

  createUser(): void {
    const dialogRef = this.dialog.open(UserFormComponent, {
      width: '550px',
      data: {user: {}}
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result != undefined) {
        this.usersDatasource.data.push(result);
        this.usersDatasource.connect().next(this.usersDatasource.data);
      }
    });
  }

  selectUser(user: User): void {
    if (this.currentUser != user) {
      this.currentUser = this.usersDatasource.data[this.getUserIndex(user)];
      this.usersDatasource.connect().next(this.usersDatasource.data);
    }
  }

  getUserIndex(user: User): number {
    return this.usersDatasource.data.indexOf(user);
  }

}

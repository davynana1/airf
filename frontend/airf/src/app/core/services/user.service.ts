import {Injectable} from '@angular/core';
import {User} from "../models/user.model";
import {elementAt, Observable} from "rxjs";
import {environment} from "../../../environments/environment";
import {HttpClient, HttpEvent, HttpHeaders, HttpResponse} from "@angular/common/http";
import {Gender} from "../../shared/enums/gender.enum";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private resourceUrl = `${environment.transfertProtocol}://${environment.serverApiUrl}/api/${environment.apiVersion}`;

  constructor(private http: HttpClient) { }

  getAllUsers(): Observable<HttpResponse<Array<User>>> {
    return this.http.get<HttpResponse<Array<User>>>(`${this.resourceUrl}/users`);
  }

  getUser(id: number): Observable<HttpResponse<User>> {
    return this.http.get<HttpResponse<User>>(`${this.resourceUrl}/user/${id}`);
  }

  saveUser(user: User): Observable<number> {
    return this.http.post<number>(`${this.resourceUrl}/user`, user);
  }

  deleteUser(id: number): void {
    this.http.delete(`${this.resourceUrl}/user/${id}`)
      .subscribe(element => {
        console.log('element ' + element)
      });
  }

  updateUser(user: User): Observable<HttpResponse<boolean>> {
    let userr: User = {
      'username': 'davy',
      'birthdate': new Date(1994,12,12),
      'residenceCountry': 'france',
      'phoneNumber': '0781735523',
      'gender': Gender.MR,
    };
    console.log('localhost:8088/api/v1/user');
    return this.http.put<HttpResponse<boolean>>(`${this.resourceUrl}/user`, userr);
  }

}

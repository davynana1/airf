import {Gender} from "../../shared/enums/gender.enum";

export interface User {

  id?: number,

  username: string,

  birthdate: Date,

  residenceCountry: string,

  phoneNumber?: string,

  gender?: Gender,

}

export class ASA{
  constructor(private caezf:number) {
  }
}

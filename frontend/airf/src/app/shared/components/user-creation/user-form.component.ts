import {Component, Inject, OnInit} from "@angular/core";
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {UserService} from "../../../core/services/user.service";
import {User} from "../../../core/models/user.model";
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  ValidationErrors,
  ValidatorFn,
  Validators
} from "@angular/forms";
import {Gender} from "../../enums/gender.enum";

@Component({
  selector: 'airf-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss']
})
export class UserFormComponent implements OnInit{


  constructor(public dialogRef: MatDialogRef<UserFormComponent>,
              private userService: UserService,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private formBuilder: FormBuilder,
  ) {}

  user: User = this.data.user;
  userForm: FormGroup = {} as FormGroup;


  get usernameCtrl(): AbstractControl {
    return this.userForm.controls['username'];
  }

  get birthdateCtrl(): AbstractControl {
    return this.userForm.controls['birthdate'];
  }

  get residenceCountryCtrl(): AbstractControl {
    return this.userForm.controls['residenceCountry'];
  }

  get phoneNumberCtrl(): AbstractControl {
    return this.userForm.controls['phoneNumber'];
  }

  get genderCtrl(): AbstractControl {
    return this.userForm.controls['gender'];
  }

  ngOnInit() {

    this.userForm = this.formBuilder.group({
      username: [this.user.username, [Validators.required, Validators.minLength(4), noSpaceAllowed]],
      birthdate: [this.user.birthdate, Validators.required],
      residenceCountry: [this.user.residenceCountry, Validators.required],
      phoneNumber: [this.user.phoneNumber, Validators.pattern(PHONE_NUMBER_REGEX)],
      gender: [this.user.gender, genderValidator],
    });
  }



  // noSpaceAllowed(): any {
  //   return (control: FormControl) => {
  //     if (control.value !== null && control.value.indexOf(' ') !== -1) {
  //       return {noSpaceAllowed: true};
  //     } else {
  //       return null;
  //     }
  //   }
  // }

  save(user: User): void {
    if(this.userForm.valid) {
      this.userService.saveUser(user).subscribe(
        userId => {
          user.id = userId;
        }
      );
    }
  }

  update(user: User): void {

  }

  cancel(): void {
    this.dialogRef.close();
  }

}

export const PHONE_NUMBER_REGEX = "^0[6|7][0-9]{8}$";


export function genderValidator(formControl: FormControl): any {
  if (formControl.value !== null && formControl.value === Gender.ROBOT) {
    return {genderValidator: true};
  } else {
    return null;
  }
}

export function noSpaceAllowed(abstractControl: AbstractControl): ValidatorFn {
  return (control: AbstractControl): ValidationErrors | null => {
    if (control.value !== null && control.value.indexOf(' ') !== -1) {
      console.log('-');
      return {noSpaceAllowed: true};
    } else {
      return null;
    }
  }
}

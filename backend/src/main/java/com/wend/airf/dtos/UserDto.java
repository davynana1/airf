package com.wend.airf.dtos;

import com.wend.airf.enums.Gender;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class UserDto {

    private Long id;

    private String username;

    private Date birthdate;

    private String residenceCountry;

    private String phoneNumber;

    private Gender gender;

}

package com.wend.airf.services;

import com.wend.airf.dtos.UserDto;
import com.wend.airf.entities.User;
import com.wend.airf.exceptions.UserException;
import com.wend.airf.exceptions.UserNotFoundException;
import com.wend.airf.mappers.UserMapper;
import com.wend.airf.repositories.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@AllArgsConstructor
@Service
public class UserService {

    private UserRepository userRepository;

    public UserDto findById(Long id) throws UserException, UserNotFoundException {
        User user = userRepository.findById(id).orElseThrow(UserNotFoundException::new);
        return UserMapper.INSTANCE.toDto(user);
    }

    public List<UserDto> findAll() throws UserNotFoundException {
        List<User> userList = userRepository.findAll();
        if (userList == null){
            throw new UserNotFoundException();
        }
        return userList.stream().map(UserMapper.INSTANCE::toDto).toList();
    }

    public User saveUser(UserDto userDto) throws UserException {
        return userRepository.save(UserMapper.INSTANCE.toEntity(userDto));
    }

    public void deleteById(Long id) {
        this.userRepository.deleteById(id);
    }
}

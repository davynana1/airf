package com.wend.airf;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AirfApplication {

	public static void main(String[] args) {
		SpringApplication.run(AirfApplication.class, args);
	}

}

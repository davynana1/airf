package com.wend.airf.abstracts;

import lombok.Data;

import javax.persistence.MappedSuperclass;
import java.io.Serial;
import java.io.Serializable;
import java.util.Date;

@Data
@MappedSuperclass
public abstract class BaseEntity implements Serializable {

    @Serial
    private static final long serialVersionUID = 1L;

    private Date creationDate;

    private Date lastModificationDate;

}

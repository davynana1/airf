package com.wend.airf.validators;

import com.wend.airf.dtos.UserDto;
import org.springframework.stereotype.Component;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;

@Component
public class UserValidator {

    public List<String> validateUserDto(UserDto userDto) {
        List<String> errors = new ArrayList<>();

        final int MAJORITY_AGE = 18;
        final String PHONE_NUMBER_REGEX = "^0[6|7][0-9]{8}$";

        if (userDto == null){
            errors.add("user is null");
        } else {
            if (ChronoUnit.YEARS.between(LocalDate.of(userDto.getBirthdate().getDay(), userDto.getBirthdate().getMonth(), userDto.getBirthdate().getYear()), LocalDate.now()) > MAJORITY_AGE) {
                errors.add("user must be an adult");
            }
            if (!Pattern.matches(PHONE_NUMBER_REGEX, userDto.getPhoneNumber())) {
                errors.add("user phone number is invalid");
            }
        }

        return errors;

    }

}

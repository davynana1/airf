package com.wend.airf.controllers;

import com.wend.airf.dtos.UserDto;
import com.wend.airf.exceptions.UserException;
import com.wend.airf.exceptions.UserNotFoundException;
import com.wend.airf.services.UserService;
import lombok.AllArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@AllArgsConstructor
@RestController
@CrossOrigin(originPatterns = "*")
@RequestMapping("api/v1/")
public class UserController {

    private UserService userService;

    @PostMapping("user")
    public ResponseEntity<Long> createAccount(@RequestBody UserDto userDto){
        HttpStatus status;
        Long userId = -1L;
        try {
            userId = userService.saveUser(userDto).getId();
            status = HttpStatus.CREATED;
        } catch (UserException userException){
            status = HttpStatus.BAD_REQUEST;
        }
        return ResponseEntity.status(status).body(userId);
    }

    @PutMapping("user")
    public ResponseEntity<Long> updateAccount(@RequestBody UserDto userDto){
        HttpStatus status;
        Long userId = -1L;
        try {
            userService.saveUser(userDto);
            status = HttpStatus.CREATED;
        } catch (UserException userException){
            status = HttpStatus.BAD_REQUEST;
        }
        return ResponseEntity.status(status).body(userId);
    }

    @GetMapping("user/{id}")
    public ResponseEntity<UserDto> getAccount(@PathVariable Long id) throws UserException {
        UserDto userDto = null;
        HttpStatus status;
        try {
            userDto = userService.findById(id);
            status = HttpStatus.OK;
        } catch (UserNotFoundException userNotFoundException) {
            status = HttpStatus.NOT_FOUND;
        }
        return ResponseEntity.status(status).body(userDto);
    }

    @DeleteMapping("user/{id}")
    public void deleteUser(@PathVariable Long id) {
        this.userService.deleteById(id);
    }

    @GetMapping("users")
    public ResponseEntity<List<UserDto>> getAllAccounts() {
        HttpStatus status;
        List<UserDto> userDtoList = null;
        try {
            userDtoList = userService.findAll();
            status = HttpStatus.OK;
        } catch (UserNotFoundException userNotFoundException) {
            status = HttpStatus.NOT_FOUND;
        }
        return ResponseEntity.status(status).body(userDtoList);
    }

    @GetMapping("userStructure")
    public UserDto getUserDtoStructure(){
        return new UserDto();
    }

}

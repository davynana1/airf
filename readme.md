# AIRF

## Description 
Petite application sans réel but fonctionnel

## Installation 
* clonez le projet
* installez les dépendances
* lancez les services présents dans postgres.yml

## Stack technique 

### Backend

Projet généré depuis Spring boot initializer (https://start.spring.io/)

| Name        | Version | Details              |
|-------------|---------|----------------------|
| Spring-boot | 2.6.6   | Back framework       |
| Hibernate   | 6.1.3   | Database manager     |
| Maven       | 3.10.1  | back package manager |
| Tomcat      | 9.0.60  | Web server           |
| Java        | 17      | Backend language     |
| Lombok      | 1.18    | Backend language     |

### Frontend

Projet généré depuis l'angular CLI (commande `ng new`)

| Name       | Version | Details           |
|------------|---------|-------------------|
| Angular    | 13.3.9  | Framework         |
| Nodejs     | 16.14.0 | Web Server        |
| Npm        | 8.3.1   | Package Manager   |
| Typescript | 4.7.4   | Frontend language |
| rxjs       | 7.5.0   | Frontend language |
| material   | 13.3.9  | Frontend language |


## Sources
* https://docs.spring.io/spring-cloud-skipper/docs/1.0.0.BUILD-SNAPSHOT/reference/html/skipper-database-configuration.html
